package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {


    @Test
    public void testDecreasecounterOne() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Equal", 0, k);

    }

    @Test
    public void testDecreasecounterTwo() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("Equal", 1, k);

    }

    @Test
    public void testDecreasecounterThree() throws Exception {

        int k= new Increment().decreasecounter(2);
        assertEquals("Equal", 2, k);

    }

}

